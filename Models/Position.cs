﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Course.Models
{
    public class Position
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string CBO { get; set; }
    }
}
