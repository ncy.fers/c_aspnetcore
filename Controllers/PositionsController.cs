﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Course.Models;

namespace Course.Controllers
{
    public class PositionsController : Controller
    {
        public IActionResult Index()
        {
            List<Position> list = new List<Position>();            
            list.Add(new Position { Id = 1, Description = "System Analyst", CBO = "212405" });
            list.Add(new Position { Id = 2, Description = "Software Engineer", CBO = "212215" });

            return View(list);
        }
    }
}